package com.carlosdavid.appmis.Notificacoes;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.carlosdavid.appmis.Enums.TipoNoticacao;
import com.carlosdavid.appmis.R;

import java.net.URI;

/**
 * Created by Carlos David on 20/03/2016.
 */
public final class Notificacao {
    String tickeer;
    String tituloNotificacao;
    String[] textosNotificacao;
    Context context;

    public final void EnviarNotificacao(Context context,String tickeer,String tituloNotificacao,String[]textosNotificacao,Intent intent)
    {
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setTicker(tickeer);
        builder.setContentTitle(tituloNotificacao);
        builder.setSmallIcon(R.drawable.church);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.icone64x64));
        builder.setContentIntent(pendingIntent);

        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle(builder);
        for(int i=0;i < textosNotificacao.length;i++)
        {
            style.addLine(textosNotificacao[i]);
        }
        builder.setStyle(style);

        Notification notification = builder.build();
        notification.vibrate = new long[]{150,300,150,600};
        notification.flags=Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(R.drawable.church,notification);

        //toque
        try{
            Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone toque = RingtoneManager.getRingtone(context,som);
            toque.play();
        }catch (Exception e)
        {

        }



    }
}
