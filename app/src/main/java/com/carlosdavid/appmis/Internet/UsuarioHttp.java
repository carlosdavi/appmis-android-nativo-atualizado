package com.carlosdavid.appmis.Internet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.carlosdavid.appmis.Entidades.Usuario;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carlos David on 25/03/2016.
 */
public final class UsuarioHttp {
    public static final String JSON_USUARIO_URL= "https://drive.google.com/uc?export=download&id=0BwDFyVi2pYqxMS1Gb3ZRUWcwbDQ";
    private String link;

    /*public UsuarioHttp() {
        super();
    }

    public UsuarioHttp(String link) {
        this.link = link;
    }*/

    public static HttpURLConnection conectar(String urlArquivo) throws IOException
    {
        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo);
        HttpURLConnection httpURLConnection =(HttpURLConnection) url.openConnection();
        httpURLConnection.setReadTimeout(10 * SEGUNDOS);
        httpURLConnection.setConnectTimeout(15 * SEGUNDOS);
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(false);
        httpURLConnection.connect();
        return  httpURLConnection;
    }
public static boolean temConexao(Context context)
{
    ConnectivityManager cm = (ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = cm.getActiveNetworkInfo();
    return (networkInfo != null && networkInfo.isConnected());
}
 public static   List<Usuario> CarregarUsuario(){
     try
     {
        HttpURLConnection conexao = conectar(JSON_USUARIO_URL);
         int respota = conexao.getResponseCode();
         if(respota == HttpURLConnection.HTTP_OK)
         {
             InputStream is = conexao.getInputStream();
             JSONObject json = new JSONObject(bytesParaString(is));

             return lerJsonUsuarios(json);
         }

     }catch (Exception e){}
    return null;
 }

public static List<Usuario> lerJsonUsuarios(JSONObject json) throws JSONException{
    List<Usuario> listaUsuario = new ArrayList<>();
    JSONArray arrayUsuario = json.getJSONArray("Usuarios");
    for(int i = 0; i < arrayUsuario.length(); i++)
    {
        Usuario usuario= new Usuario();
       usuario.setNomeUsuario(arrayUsuario.getJSONObject(i).getString("nomeUsuario"));
       usuario.setSenha(arrayUsuario.getJSONObject(i).getString("senha"));
        usuario.setNumeroTell(arrayUsuario.getJSONObject(i).getString("telefone"));
        listaUsuario.add(usuario);
    }
return listaUsuario;
}
    public static String bytesParaString(InputStream is) throws IOException
    {
        byte[] buffer = new byte[1024];

        ByteArrayOutputStream buffersao = new ByteArrayOutputStream();
        int bytesLidos;
        while((bytesLidos = is.read(buffer)) != -1)
        {
            buffersao.write(buffer,0,bytesLidos);
        }

        return  new String(buffersao.toByteArray(),"UTF-8");
    }

}
