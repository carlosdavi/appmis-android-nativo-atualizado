package com.carlosdavid.appmis.Entidades;

import android.content.Context;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseApp;
import com.firebase.client.FirebaseError;

/**
 * Created by Carlos David on 26/03/2016.
 */
public final class Status implements ChildEventListener{
    private Context context;
    private Firebase refFirebase;
    private Firebase refStatus;
    private boolean activated=true;


    public Status(Context context) {
        this.context = context;

        Firebase.setAndroidContext(context);
        refFirebase = new Firebase("https://misapp.firebaseio.com/");
        refStatus = refFirebase.child("Status");
        setActivated(Boolean.getBoolean(refStatus.getKey()));
        refStatus.addChildEventListener(this);
    }

    public boolean isActivated() {
        return activated;
    }

    private void setActivated(boolean activated) {
        this.activated = activated;
    }

    public final boolean  GetStatus()
    {
        return isActivated();
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        setActivated(dataSnapshot.getValue(Boolean.class));
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        setActivated(dataSnapshot.getValue(Boolean.class));
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        setActivated(dataSnapshot.getValue(Boolean.class));
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {

    }
}
