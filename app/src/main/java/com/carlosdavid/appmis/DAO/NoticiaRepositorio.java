package com.carlosdavid.appmis.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.carlosdavid.appmis.Entidades.Noticia;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carlos David on 22-Mar-16.
 */
public class NoticiaRepositorio {
    private NoticiaSqlHelper noticiaSqlHelper;

    public NoticiaRepositorio(Context context)
    {
        noticiaSqlHelper = new NoticiaSqlHelper(context);
    }

    private long inserir(Noticia noticia)
    {
        SQLiteDatabase database = noticiaSqlHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(NoticiaSqlHelper.COLUNA_TITULO_NOTICIA, noticia.getTitulo());
        contentValues.put(NoticiaSqlHelper.COLUNA_DATA_NOTICIA, noticia.getData());
        contentValues.put(NoticiaSqlHelper.COLUNA_AUTOR_NOTICIA, noticia.getAutor());
        contentValues.put(NoticiaSqlHelper.COLUNA_IMAGEM_NOTICIA, noticia.getStringImagem());
        contentValues.put(NoticiaSqlHelper.COLUNA_ID_FIREBASE, noticia.getIdFirebase());
        contentValues.put(NoticiaSqlHelper.COLUNA_TEXTO_NOTICIA, noticia.getTextoNoticia());

        long id = database.insert(noticiaSqlHelper.TABELA_NOTICIA,null,contentValues);

        if(id != -1)
        {
            noticia.setId(id);
        }

        database.close();
        return id;
    }
    private int atualizar(Noticia noticia)
    {
        SQLiteDatabase database = noticiaSqlHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(NoticiaSqlHelper.COLUNA_TITULO_NOTICIA, noticia.getTitulo());
        contentValues.put(NoticiaSqlHelper.COLUNA_DATA_NOTICIA, noticia.getData());
        contentValues.put(NoticiaSqlHelper.COLUNA_AUTOR_NOTICIA,noticia.getAutor());
        contentValues.put(NoticiaSqlHelper.COLUNA_IMAGEM_NOTICIA,noticia.getStringImagem());
        contentValues.put(NoticiaSqlHelper.COLUNA_ID_FIREBASE,noticia.getIdFirebase());
        contentValues.put(NoticiaSqlHelper.COLUNA_TEXTO_NOTICIA, noticia.getTextoNoticia());

        int linhasAfetadas = database.update
                (
                        NoticiaSqlHelper.TABELA_NOTICIA,
                        contentValues,
                        NoticiaSqlHelper.COLUNA_ID + " = ?",
                        new String[]{String.valueOf(noticia.getId())}
                );
        database.close();
        return linhasAfetadas;
    }
    private int atualizarViaIdFirebase(Noticia noticia)
    {
        SQLiteDatabase database = noticiaSqlHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(NoticiaSqlHelper.COLUNA_TITULO_NOTICIA, noticia.getTitulo());
        contentValues.put(NoticiaSqlHelper.COLUNA_DATA_NOTICIA, noticia.getData());
        contentValues.put(NoticiaSqlHelper.COLUNA_AUTOR_NOTICIA,noticia.getAutor());
        contentValues.put(NoticiaSqlHelper.COLUNA_IMAGEM_NOTICIA,noticia.getStringImagem());
        contentValues.put(NoticiaSqlHelper.COLUNA_ID_FIREBASE,noticia.getIdFirebase());
        contentValues.put(NoticiaSqlHelper.COLUNA_TEXTO_NOTICIA, noticia.getTextoNoticia());

        int linhasAfetadas = database.update
                (
                        NoticiaSqlHelper.TABELA_NOTICIA,
                        contentValues,
                        NoticiaSqlHelper.COLUNA_ID_FIREBASE + " = ?",
                        new String[]{String.valueOf(noticia.getIdFirebase())}
                );
        database.close();
        return linhasAfetadas;
    }


    public void salvar(Noticia noticia)
    {
        if(noticia.getId() == 0)
        {
            inserir(noticia);
        }
        else
        {
            atualizar(noticia);
        }
    }
    public int excluir(Noticia noticia)
    {
        SQLiteDatabase database = noticiaSqlHelper.getWritableDatabase();
        int linhasAfetadas = database.delete
                (
                        NoticiaSqlHelper.TABELA_NOTICIA,
                        NoticiaSqlHelper.COLUNA_ID +" = ?",
                        new String[]{String.valueOf(noticia.getId())}

                );
        Log.e("Log-Dados Apagado:", "" + noticia.getId());
        database.close();
        return linhasAfetadas;
    }
    public int excluirViaIdFirebase(Noticia noticia)
    {
        SQLiteDatabase database = noticiaSqlHelper.getWritableDatabase();
        int linhasAfetadas = database.delete
                (
                        NoticiaSqlHelper.TABELA_NOTICIA,
                        NoticiaSqlHelper.COLUNA_ID_FIREBASE +" = ?",
                        new String[]{String.valueOf(noticia.getIdFirebase())}

                );
        Log.e("Log-Dados Apagado:", "" + noticia.getId());
        database.close();
        return linhasAfetadas;
    }

    public List<Noticia> buscaNoticia(String filtro)
    {
        SQLiteDatabase database = noticiaSqlHelper.getReadableDatabase();

        String sql = "SELECT * FROM "+ NoticiaSqlHelper.TABELA_NOTICIA;
        String[] argumentos = null;
        if(filtro != null)
        {
            sql+=" WHERE "+ NoticiaSqlHelper.COLUNA_TITULO_NOTICIA +" LIKE ?";
            argumentos = new String[]{filtro};
        }
        sql += " ORDER BY " + NoticiaSqlHelper.COLUNA_ID;
        Cursor cursor = database.rawQuery(sql,argumentos);

        List<Noticia> listaNoticias = new ArrayList<>();
        while(cursor.moveToNext())
        {
            long id = cursor.getLong(cursor.getColumnIndex(NoticiaSqlHelper.COLUNA_ID));
            String tituloNoticia  = cursor.getString(cursor.getColumnIndex(NoticiaSqlHelper.COLUNA_TITULO_NOTICIA));
            String dataNoticia = cursor.getString(cursor.getColumnIndex(NoticiaSqlHelper.COLUNA_DATA_NOTICIA));
            String autorNoticia = cursor.getString(cursor.getColumnIndex(NoticiaSqlHelper.COLUNA_AUTOR_NOTICIA));
            String imagemNoticia = cursor.getString(cursor.getColumnIndex(NoticiaSqlHelper.COLUNA_IMAGEM_NOTICIA));
            String idFirebase = cursor.getString(cursor.getColumnIndex(NoticiaSqlHelper.COLUNA_ID_FIREBASE));
            String textoNoticia = cursor.getString(cursor.getColumnIndex(NoticiaSqlHelper.COLUNA_TEXTO_NOTICIA));

            Noticia noticia = new Noticia(id,tituloNoticia,dataNoticia,autorNoticia,imagemNoticia,idFirebase,textoNoticia);
            listaNoticias.add(noticia);
        }
        cursor.close();
        database.close();
        return listaNoticias;
    }
}
