package com.carlosdavid.appmis.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.carlosdavid.appmis.Entidades.Calendario;
import com.carlosdavid.appmis.R;

import java.util.List;

/**
 * Created by Carlos David on 14/03/2016.
 */
public class CalendarioAdapter extends BaseAdapter{
    private Context context;
    private List<Calendario> listaCalendario;
    public CalendarioAdapter(){

    }
    public CalendarioAdapter(Context context, List<Calendario> listaCalendario){
        this.listaCalendario = listaCalendario;
        this.context = context;
    }
    @Override
    public int getCount() {
        return listaCalendario.size();
    }

    @Override
    public Object getItem(int i) {
        return listaCalendario.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Calendario calendario = listaCalendario.get(i);
        View layout;
        if(view == null)
        {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = inflater.inflate(R.layout.adapter_lista_calendario,null);
        }
        else
        {
            layout = view;
        }

        TextView ano = (TextView)layout.findViewById(R.id.anoCalendario);
        TextView dia = (TextView)layout.findViewById(R.id.diaCalendario);
        TextView mes = (TextView)layout.findViewById(R.id.mesCalendario);
        TextView evento = (TextView)layout.findViewById(R.id.eventoCalendario);

        ano.setText(calendario.getAno());
        dia.setText(calendario.getDia());
        mes.setText(calendario.getMes());
        evento.setText(calendario.getEvento());

        return layout;
    }
}
