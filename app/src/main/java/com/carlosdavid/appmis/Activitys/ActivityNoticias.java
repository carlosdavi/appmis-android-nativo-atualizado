package com.carlosdavid.appmis.Activitys;

import android.app.DialogFragment;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.carlosdavid.appmis.Adapters.NoticiaAdapter;
import com.carlosdavid.appmis.DAO.NoticiaRepositorio;
import com.carlosdavid.appmis.Entidades.Noticia;
import com.carlosdavid.appmis.Entidades.Status;
import com.carlosdavid.appmis.Entidades.Usuario;
import com.carlosdavid.appmis.Fragments.AlimentoDiarioDialogFragment;
import com.carlosdavid.appmis.Fragments.EntradaLiderDialogFragment;
import com.carlosdavid.appmis.Internet.UsuarioHttp;
import com.carlosdavid.appmis.Notificacoes.Notificacao;
import com.carlosdavid.appmis.R;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityNoticias extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ChildEventListener {

    ListView listViewNoticias;
    List<Noticia> listaNoticia;
    NoticiaAdapter adapter;
    Firebase ref;
    ProgressDialog progressDialog;
    NoticiaRepositorio noticiaRepositorio;
    private  List<Usuario> usuarios = new ArrayList<Usuario>();;
    private EditText login ;
    private EditText senha;
    EntradaLiderDialogFragment entradaLiderDialogFragment;
    ProgressDialog progressDialog2;
    private  String textoAlimento = " ";
    Firebase refAlimentoDiario;
    AlimentoDiarioDialogFragment alimentoDiarioDialogFragment ;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticias);


      //  if(new Status(this).GetStatus())
    //    {
        SetViewsAcativity();
        setDados();


            Firebase.setAndroidContext(this);
            ref = new Firebase("https://misapp.firebaseio.com/");

            Firebase refNoticias = ref.child("Noticia");
            refAlimentoDiario = ref.child("AlimentoDiario");
            Log.e("Script:", listaNoticia.get(0).getAutor());

        refAlimentoDiario.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                textoAlimento = dataSnapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

            refNoticias.addChildEventListener(this);

            listViewNoticias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Toast.makeText(ActivityNoticias.this, listaNoticia.get(i).getIdFirebase(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ActivityNoticias.this,Activity_Detalhe_Noticia.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("autor",listaNoticia.get(i).getAutor());
                    bundle.putString("data",listaNoticia.get(i).getData());
                    bundle.putString("titulo",listaNoticia.get(i).getTitulo());
                    bundle.putString("textoNoticia",listaNoticia.get(i).getTextoNoticia());
                    bundle.putString("stringImagem",listaNoticia.get(i).getStringImagem());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });

    //    }
  ///      else
    //    {
     //       Toast.makeText(this,"Desativado!",Toast.LENGTH_LONG).show();
     //   }



        //Log.e("valor:",listaNoticia.get(0).getIdFirebase());
      //  Log.e("valor:",listaNoticia.get(1).getIdFirebase());


        // refNoticias.setValue(listaNoticia);







    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_noticias, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            if(VeriricaConexao())
            {

                OpenDrialogAcessoLideres();
                CarregarUsuarios();

            } else
            {
                Snackbar.make((View)findViewById(R.id.tv), "Você precisa estar conectado a internet =)", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
            return true;
        }
        else if(id == R.id.alimentoDiario)
        {
            OpenAlimentoDiarioDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_noticias) {

        } else if (id == R.id.nav_calendario) {

        } else if (id == R.id.nav_igreja) {

        } else if (id == R.id.nav_onibus) {

        } else if (id == R.id.nav_utilitario) {
            Intent intent = new Intent(this, ActivityDeSorteio2.class);
            startActivity(intent);

        } else if (id == R.id.nav_sobrejump) {

        } else if (id == R.id.nav_sobredev) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void SetViewsAcativity() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        listViewNoticias = (ListView)findViewById(R.id.listViewNoticia);

       //DAO
        noticiaRepositorio = new NoticiaRepositorio(getBaseContext());



    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Carregando...");
        progressDialog.show();
        //---

        int sizeListaAnterior = listaNoticia.size();

        String auxTitulo = (dataSnapshot.getValue(Noticia.class)).getTitulo();
        String auxData = (dataSnapshot.getValue(Noticia.class)).getData();
        String auxAutor = (dataSnapshot.getValue(Noticia.class)).getAutor();
        String stringImagem = (dataSnapshot.getValue(Noticia.class)).getStringImagem();
        String idFirebase = (dataSnapshot.getValue(Noticia.class)).getIdFirebase();
        String textoNoticia = (dataSnapshot.getValue(Noticia.class)).getTextoNoticia();

        Noticia noticia = new Noticia(auxTitulo, auxData, auxAutor, stringImagem,idFirebase,textoNoticia);

        if(!ExisteNoDB(noticia))
        {
            noticiaRepositorio.salvar(noticia);
            Log.e("SCRIPT:","SAVE EXECUTED!!!!!");
            listaNoticia.add(noticia);
        }


        if (listaNoticia.size() > sizeListaAnterior) {
            new Notificacao().EnviarNotificacao
                    (
                            this,
                            "Você recebeu uma nova Notícia",
                            auxTitulo,
                            new String[]{""},
                            new Intent(this, ActivityNoticias.class)
                    );

            adapter.notifyDataSetChanged();

        }
        progressDialog.dismiss();


    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        Log.i("Script:", "onChildChanged");
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
       Noticia noticiaExcluida = dataSnapshot.getValue(Noticia.class);
        noticiaRepositorio.excluirViaIdFirebase(noticiaExcluida);
        for(int i=0; i < listaNoticia.size(); i++)
        {
            if(listaNoticia.get(i).getIdFirebase().equals(noticiaExcluida.getIdFirebase()))
            {
                listaNoticia.remove(listaNoticia.get(i));
            }else{}
        }
       // listaNoticia.remove(Integer.parseInt(index));
        adapter.notifyDataSetChanged();

        Log.i("Script:", "onChildRemoved");
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        Log.i("Script:", "onChildMoved");
    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {
        Log.i("Script:", "onCancelled");
    }


    public boolean VeriricaConexao() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if ((networkInfo != null) && (networkInfo.isConnected())) {
            return true;
        } else {
            return false;
        }
    }
    public void OpenDrialogAcessoLideres()
    {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        entradaLiderDialogFragment = new EntradaLiderDialogFragment();
        entradaLiderDialogFragment.show(fragmentTransaction, "EntradaLiderDialog");
    }
    public void CancelarAcessoLider(View view)
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        EntradaLiderDialogFragment entradaLiderDialogFragment = (EntradaLiderDialogFragment)getSupportFragmentManager().findFragmentByTag("EntradaLiderDialog");
        if(entradaLiderDialogFragment != null)
        {
            entradaLiderDialogFragment.dismiss();
            fragmentTransaction.remove(entradaLiderDialogFragment);
        }
    }
    public void LogarAcessoLider(View view)
    {
        if(VeriricaConexao())

        {
            ValidarAcesso();
        }
        else
        {
            Snackbar.make(view, "Você precisa estar conectado a internet =)", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }

    }

    public void getSolicitarAcesso()
    {

            Intent intent = new Intent(ActivityNoticias.this,Activity_Relatorio.class);
            Bundle bundle = new Bundle();
            bundle.putString("link","https://docs.google.com/forms/d/1gKvk-zSXPH3Zye1XsddBk0IfnWhPipmG9cdwS14brk4/viewform");
            intent.putExtras(bundle);
            startActivity(intent);


    }
    public void getRelatorio( )
    {

            Intent intent = new Intent(ActivityNoticias.this,Activity_Relatorio.class);
            Bundle bundle = new Bundle();
            bundle.putString("link","https://docs.google.com/forms/d/1TJ12KdxzrtUdKduubr_vyTzC0Lz9TDNpyIUinhp2haY/viewform");
            intent.putExtras(bundle);
            startActivity(intent);

    }
    public void getAcessoRelatorio(View view)
    {
        getSolicitarAcesso();
    }

    private void setDados(){
        //select * no SqLite
        List<Noticia> listaDeNoticias = noticiaRepositorio.buscaNoticia(null);

        if((listaDeNoticias.isEmpty()) ||(listaDeNoticias == null))
        {
            this.listaNoticia = new ArrayList<>();
            listaNoticia.add(new Noticia(0," "," "," "," "," "));
        }
        else
        {
            this.listaNoticia =  listaDeNoticias;
        }

        adapter = new NoticiaAdapter(getBaseContext(),this.listaNoticia);

        this.listViewNoticias.setAdapter(adapter);
    }
    private boolean ExisteNoDB(Noticia noticia){
        List<Noticia> noticiasDoDB = noticiaRepositorio.buscaNoticia(null);
        boolean resposta = false;

        for(Noticia noticiaDB : noticiasDoDB)
        {
             if(noticiaDB.getTitulo().equals( noticia.getTitulo()))
             {

                 return true;
             }
            else{
                 resposta = false;
             }
        }
        return resposta;

    }



    private void ValidarAcesso()
    {

        login = ((EditText)entradaLiderDialogFragment.getView().findViewById(R.id.edtNomeUsuario));
        senha = ((EditText)entradaLiderDialogFragment.getView().findViewById(R.id.edtSenhaUsuario));

            for(Usuario usuario : usuarios)
            {
                if((usuario.getNomeUsuario().equals(login.getText().toString())) && (usuario.getSenha().equals(senha.getText().toString())))
                {
                    getRelatorio();
                    break;
                }
                else
                {
                        Toast.makeText(ActivityNoticias.this,"Usuário ou senha inválidos!",Toast.LENGTH_SHORT).show();
                }
            }
        }

public void CloseAlimentoDiarioDialog(View view)
{
    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
    AlimentoDiarioDialogFragment alimentoDiarioDialogFragment =(AlimentoDiarioDialogFragment) getSupportFragmentManager().findFragmentByTag("AlimentoDiarioDialog");
    if(alimentoDiarioDialogFragment != null){
        alimentoDiarioDialogFragment.dismiss();
        fragmentTransaction.remove(alimentoDiarioDialogFragment);
    }




}
    public void OpenAlimentoDiarioDialog()
    {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        alimentoDiarioDialogFragment = new AlimentoDiarioDialogFragment(1,2);
        alimentoDiarioDialogFragment.show(fragmentTransaction, "AlimentoDiarioDialog");
        //CarregaAlimentoDiario();

    }
    public void CarregaAlimentoDiario(View view)
    {


        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        AlimentoDiarioDialogFragment alimentoDiarioDialogFragment = (AlimentoDiarioDialogFragment)
                getSupportFragmentManager().findFragmentByTag("AlimentoDiarioDialog");

        TextView tv = (TextView)alimentoDiarioDialogFragment.getView().findViewById(R.id.textoAlimentoDiario);
        tv.setText(this.textoAlimento);
    }

    public void CarregarUsuarios()
    {
        Firebase refUsuarios = ref.child("Usuario");
        refUsuarios.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s)
            {
                usuarios.add(dataSnapshot.getValue(Usuario.class));
           }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                usuarios = new ArrayList<Usuario>();
                usuarios.add(dataSnapshot.getValue(Usuario.class));
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                usuarios = new ArrayList<Usuario>();
                usuarios.add(dataSnapshot.getValue(Usuario.class));
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }


}
