package com.carlosdavid.appmis.Activitys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.carlosdavid.appmis.R;

public class Activity_Relatorio extends AppCompatActivity {
    WebView webView;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });



        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        webView = (WebView)findViewById(R.id.webViewMIS);
        WebSettings ws =  webView.getSettings();
        ws.setJavaScriptEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Carregando Relatório...");
        progressDialog.show();
        //https://docs.google.com/forms/d/1TJ12KdxzrtUdKduubr_vyTzC0Lz9TDNpyIUinhp2haY/viewform
        webView.loadUrl(bundle.getString("link"));

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        progressDialog.dismiss();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        int id = menuItem.getItemId();
        switch (id)
        {
            case android.R.id.home:
                finish();
                return true;


        }
        return true;
    }

}
