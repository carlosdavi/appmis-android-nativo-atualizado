package com.carlosdavid.appmis.Activitys;

import android.content.Intent;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.carlosdavid.appmis.R;
import com.carlosdavid.appmis.Tratamento_de_Imagens.TratarImagem;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Activity_Detalhe_Noticia extends AppCompatActivity implements  View.OnClickListener {
    Bundle bundle;
    Bitmap bm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_noticia);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


       // FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
       // fab.setOnClickListener(this);

        Intent intent = getIntent();
         bundle = intent.getExtras();
        ((TextView)findViewById(R.id.tvTituloNoticia)).setText(bundle.getString("titulo"));
        ((TextView)findViewById(R.id.AutorNoticia)).setText(bundle.getString("autor"));
        ((TextView)findViewById(R.id.textDataNoticia)).setText(bundle.getString("data"));
        ((TextView)findViewById(R.id.textoDaNoticia)).setText(bundle.getString("textoNoticia"));

        //Imagem
        CarregarImagem(R.id.imagemTextoNoticia);

    }

    @Override
    public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        int id = menuItem.getItemId();
        switch (id)
        {
            case android.R.id.home:
                finish();
                return true;


        }
        return true;
    }

    private Bitmap getImage(String imageUrl, int desiredWidth, int desiredHeight)
    {
         Bitmap image = null;
        int inSampleSize = 0;


        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;

        options.inSampleSize = inSampleSize;

        try
        {
            URL url = new URL(imageUrl);

            HttpURLConnection connection = (HttpURLConnection)url.openConnection();

            InputStream stream = connection.getInputStream();

            image = BitmapFactory.decodeStream(stream, null, options);

            int imageWidth = options.outWidth;

            int imageHeight = options.outHeight;

            if(imageWidth > desiredWidth || imageHeight > desiredHeight)
            {
                System.out.println("imageWidth:"+imageWidth+", imageHeight:"+imageHeight);

                inSampleSize = inSampleSize + 2;

              //  getImage(imageUrl,desiredWidth,desiredHeight);
            }
           // else
           // {
                options.inJustDecodeBounds = false;

                connection = (HttpURLConnection)url.openConnection();

                stream = connection.getInputStream();

                image = BitmapFactory.decodeStream(stream, null, options);

                return image;
           // }
        }

        catch(Exception e)
        {
            Log.e("getImage", e.toString());
        }

        return image;
    }
    private void CarregarImagem(final int idImagem)
    {
        if(VeriricaConexao()){


        new Thread(new Runnable() {
            @Override
            public void run() {

                bm =(getImage(bundle.getString("stringImagem"), getWidth(), 100));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((ImageView)findViewById(idImagem)).setImageBitmap(bm);
                    }
                });

            }
        }).start();
        }else
        {
            Log.e("Script:","Sem conexão. não baixará a imagem");
        }

    }
    private int getWidth()
    {
        WindowManager windowManager =(WindowManager) this.getSystemService(WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        return  display.getWidth();

    }

    public boolean VeriricaConexao() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if ((networkInfo != null) && (networkInfo.isConnected())) {
            return true;
        } else {
            return false;
        }
    }
}
