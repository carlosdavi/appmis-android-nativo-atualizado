package com.carlosdavid.appmis.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Carlos David on 22-Mar-16.
 */
public class NoticiaSqlHelper extends SQLiteOpenHelper{
    private static final String NOME_BANCO = "dbAppMIS";
    private static final int VERSAO_BANCO = 1;
     static final String TABELA_NOTICIA= "noticias";
    public static final String COLUNA_ID = "_id";
    public static final String COLUNA_TITULO_NOTICIA = "tituloNoticia";
    public static final String COLUNA_DATA_NOTICIA = "dataNoticia";
    public static final String COLUNA_AUTOR_NOTICIA = "autorNoticia";
    public static final String COLUNA_IMAGEM_NOTICIA = "imagemNoticia";
    public static final String COLUNA_ID_FIREBASE = "idFirebase";
    public static final String COLUNA_TEXTO_NOTICIA = "textoNoticia";

    public NoticiaSqlHelper(Context context)
    {
        super(context,NOME_BANCO,null,VERSAO_BANCO);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        sqLiteDatabase.execSQL
                (
                        "CREATE TABLE "+TABELA_NOTICIA+ " ("+
                        COLUNA_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                                COLUNA_TITULO_NOTICIA +" TEXT NOT NULL,"+
                                COLUNA_DATA_NOTICIA +" TEXT,"+
                                COLUNA_AUTOR_NOTICIA +" TEXT NOT NULL,"+
                                COLUNA_IMAGEM_NOTICIA+" TEXT,"+
                                COLUNA_ID_FIREBASE+" TEXT,"+
                                COLUNA_TEXTO_NOTICIA+" TEXT)"
                );
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //PARA PRÓXIMAS VERSÕES
    }

}
