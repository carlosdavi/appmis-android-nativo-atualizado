package com.carlosdavid.appmis.Activitys;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.carlosdavid.appmis.R;

import java.util.Random;

public class ActivityDeSorteio2 extends AppCompatActivity  {

    ProgressDialog progressDialog;
    TextView tvNumero;

    EditText edtFim;
    int numeroSorteado;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_de_sorteio2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

       // progressBar = (ProgressBar)findViewById(R.id.progressSorteio);
        tvNumero = (TextView)findViewById(R.id.numeroSorteado);

        edtFim = (EditText)findViewById(R.id.edtFim);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        int id = menuItem.getItemId();
        switch (id)
        {
            case android.R.id.home:
                finish();
                return true;


        }
        return true;
    }
    public void Sortear(View view){
        //progressBar.setVisibility(View.VISIBLE);


        String aux = ((edtFim.getText().toString()));
        if(edtFim.getText().toString().equals(""))
        {
            MostraSnackbar("Aviso: Entrar com valor válido.",view);
        }

        else
        {
            Random sort = new Random();
            numeroSorteado = sort.nextInt(Integer.parseInt(aux)+1);
            progressDialog = new ProgressDialog(ActivityDeSorteio2.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Sorteando...");
            progressDialog.show();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            tvNumero.setText(Integer.toString(numeroSorteado));
                            Thread.interrupted();
                        }
                    });
                    Thread.interrupted();
                }
            }).start();

        }


    }


    public void MostraSnackbar(String msg,View view) {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

    }
}
