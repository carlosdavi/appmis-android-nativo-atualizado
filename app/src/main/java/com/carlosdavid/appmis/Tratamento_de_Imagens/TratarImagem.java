package com.carlosdavid.appmis.Tratamento_de_Imagens;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

/**
 * Created by Carlos David on 14/03/2016.
 */
public final class TratarImagem {

    public static Bitmap DecodificaImagem(String stringImagem)
    {
        byte[] byteArray = Base64.decode(stringImagem, Base64.DEFAULT);//   Resource(getResources(), R.drawable.chiken);//your image
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        if(bmp != null)
        {
            Log.e("Script:", "Imagem Retornada!");
            return bmp;

        }else
        {
            Log.e("Script:", "Erro ao Retornar imagem! - String imagem: \n"+stringImagem);
            return null;

        }

    }
}
