package com.carlosdavid.appmis.Entidades;

/**
 * Created by Carlos David on 14/03/2016.
 */
public class Culto {
    private String dia;
    private String hora;
    private String descricao;

    public Culto(String dia, String hora, String descricao) {
        this.dia = dia;
        this.hora = hora;
        this.descricao = descricao;
    }

    public Culto() {
        super();
    }

    public String getDia() {
        return dia;
    }

    public String getHora() {
        return hora;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
