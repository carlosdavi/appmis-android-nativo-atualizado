package com.carlosdavid.appmis.Entidades;

/**
 * Created by Carlos David on 14/03/2016.
 */
public class Calendario {
    private String evento;
    private int dia;
    private int mes;
    private int ano;

    public  Calendario() {
        super();
    }
    public  Calendario(int dia, int mes, int ano, String evento)
    {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        this.evento = evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }



    public String getEvento() {
        return evento;
    }

    public int getDia() {
        return dia;
    }

    public int getMes() {
        return mes;
    }

    public int getAno() {
        return ano;
    }





        }


