package com.carlosdavid.appmis.Activitys;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.speech.RecognizerIntent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.carlosdavid.appmis.R;

import java.net.URI;

public class TesteNotification extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teste_notification);
    }

    public void Notificar(View view){
        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,ActivityDeSorteio2.class),0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setTicker("Ticker text"); //Texto que aparece rapido na notificacao
        builder.setContentTitle("Titulo");

        //builder.setContentText("Descricao");
        builder.setSmallIcon(R.drawable.church);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.calendar));
        builder.setContentIntent(pendingIntent);

        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
        String[] descs = new String[]{"Descricao 1","Descricao 2","Descricao 3","Descricao 4"};
        for(int i=0;i<descs.length;i++){
            style.addLine(descs[i]);
        }
        builder.setStyle(style);

        Notification notification = builder.build();
        notification.vibrate  = new long[]{150,300,150,600};
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(R.drawable.church,notification);

        //toque
        try{
            Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone toque = RingtoneManager.getRingtone(this,som);
            toque.play();
        }catch (Exception e){}

    }
}
