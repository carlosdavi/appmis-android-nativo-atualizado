package com.carlosdavid.appmis.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.carlosdavid.appmis.Entidades.Noticia;
import com.carlosdavid.appmis.R;
import com.carlosdavid.appmis.Tratamento_de_Imagens.TratarImagem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carlos David on 14/03/2016.
 */
public class NoticiaAdapter extends BaseAdapter{
    private Context context;
    private List<Noticia> listaNoticia;


    public NoticiaAdapter(Context context, List<Noticia> listaNoticia) {
        this.context = context;
        this.listaNoticia = listaNoticia;
    }
    public NoticiaAdapter() {
       super();
    }

    @Override
    public int getCount() {
        return listaNoticia.size();
    }

    @Override
    public Object getItem(int i) {
        return listaNoticia.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Noticia noticia = listaNoticia.get(i);
        View layout;
        if(view == null)
        {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = inflater.inflate(R.layout.adapter_lista_noticia,null);
        }
        else
        {
            layout = view;
        }

        ImageView imagemNoticia = (ImageView)layout.findViewById(R.id.imagemNoticia);
        TextView tituloNoticia = (TextView)layout.findViewById(R.id.tituloNoticia);
        TextView autorNoticia = (TextView)layout.findViewById(R.id.autorNoticia);
        TextView dataNoticia = (TextView)layout.findViewById(R.id.dataNoticia);

        //imagemNoticia.setImageBitmap(TratarImagem.DecodificaImagem(noticia.getStringImagem()));
        tituloNoticia.setText(noticia.getTitulo());
        autorNoticia.setText(noticia.getAutor());
        dataNoticia.setText(noticia.getData());



        return layout;
    }
}
