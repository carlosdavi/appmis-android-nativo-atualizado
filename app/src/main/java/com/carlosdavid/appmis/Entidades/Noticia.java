package com.carlosdavid.appmis.Entidades;

/**
 * Created by Carlos David on 14/03/2016.
 */
public class Noticia {
    private long id;
    private String titulo;
    private String data;
    private String autor;
    private String stringImagem;
    private String idFirebase;
    private String textoNoticia;


    public Noticia(String titulo, String data, String autor, String stringImagem, String idFirebase,String textoNoticia)
    {
        this.titulo = titulo;
        this.data = data;
        this.autor = autor;
        this.stringImagem = stringImagem;
        this.idFirebase = idFirebase;
        this.textoNoticia = textoNoticia;
    }
    public Noticia(long id,String titulo, String data, String autor, String stringImagem,String textoNoticia)
    {
        this.id = id;
        this.titulo = titulo;
        this.data = data;
        this.autor = autor;
        this.stringImagem = stringImagem;
        this.textoNoticia = textoNoticia;
    }
    public Noticia(long id,String titulo, String data, String autor, String stringImagem,String idFirebase,String textoNoticia)
    {
        this.id = id;
        this.titulo = titulo;
        this.data = data;
        this.autor = autor;
        this.stringImagem = stringImagem;
        this.idFirebase = idFirebase;
        this.textoNoticia = textoNoticia;

    }
    public Noticia(){
        super();
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setStringImagem(String stringImagem) {
        this.stringImagem = stringImagem;
    }



    public String getStringImagem() {
        return stringImagem;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getData() {
        return data;
    }

    public String getAutor() {
        return autor;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdFirebase() {
        return idFirebase;
    }

    public String getTextoNoticia() {
        return textoNoticia;
    }

    public void setTextoNoticia(String textoNoticia) {
        this.textoNoticia = textoNoticia;
    }

    public void setIdFirebase(String idFirebase) {
        this.idFirebase = idFirebase;
    }
}
