package com.carlosdavid.appmis.Entidades;

/**
 * Created by Carlos David on 14/03/2016.
 */
public class ItinerarioOnibus {
    private String nomeOnibus;
    private String rota;
    private String stringImagemOnibus;


    public void setNomeOnibus(String nomeOnibus) {
        this.nomeOnibus = nomeOnibus;
    }

    public void setRota(String rota) {
        this.rota = rota;
    }

    public void setStringImagemOnibus(String stringImagemOnibus) {
        this.stringImagemOnibus = stringImagemOnibus;
    }

    public String getNomeOnibus() {
        return nomeOnibus;
    }

    public String getRota() {
        return rota;
    }

    public String getStringImagemOnibus() {
        return stringImagemOnibus;
    }



    public  ItinerarioOnibus(){
       super();
    }
    public  ItinerarioOnibus(String nomeOnibus,String rota, String StringImagemOnibus){
        this.nomeOnibus = nomeOnibus;
        this.rota = rota;
        this.stringImagemOnibus = stringImagemOnibus;
    }
}
