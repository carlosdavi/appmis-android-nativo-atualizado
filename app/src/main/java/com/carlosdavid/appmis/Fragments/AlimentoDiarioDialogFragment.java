package com.carlosdavid.appmis.Fragments;


import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.carlosdavid.appmis.Activitys.ActivityNoticias;
import com.carlosdavid.appmis.R;

/**
 * Created by Carlos David on 22/03/2016.
 */
public class AlimentoDiarioDialogFragment extends DialogFragment {
    private int numStyle;
    private int numTheme;
    public AlimentoDiarioDialogFragment(){
        super();
    }

    public AlimentoDiarioDialogFragment(int numStyle, int numTheme)
    {
        this.numStyle = numStyle;
        this.numTheme = numTheme;
    }

    @Override
    public void onCreate(Bundle savedInstanteState)
    {
        super.onCreate(savedInstanteState);
        Log.i("Script:", "onCreate");
        int style;
        int theme;
        switch (numStyle)
        {
            case 1:style = DialogFragment.STYLE_NO_TITLE; break;
            case 2:style = DialogFragment.STYLE_NO_INPUT; break;
            case 3:style = DialogFragment.STYLE_NO_FRAME;break;
            default:style = DialogFragment.STYLE_NORMAL;break;
        }
        switch (numTheme)
        {
            case 1:theme = android.R.style.Theme_Holo; break;
            case 2:theme = android.R.style.Theme_Holo_Dialog; break;
            case 3:theme = android.R.style.ThemeOverlay_Material_Dark_ActionBar; break;
            default:theme = android.R.style.Theme_Holo_Light_DarkActionBar;break;
        }

        setStyle(style,theme);
        setCancelable(true);


    }
     @Override
   public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanteState){
        super.onCreateView(inflater, container, savedInstanteState);
        View view = inflater.inflate(R.layout.dialog_layout_alimento_diario,container);


       /*  TextView btExit = (TextView)view.findViewById(R.id.tvVoltar);
        btExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  dismiss();

                 ((ActivityNoticias)getActivity()).CloseAlimentoDiarioDialog(view);
            }
        });*/


        return(view);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        Log.i("Script:", "onActivityCreated");

    }
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        Log.i("Script:","onAttach");

    }
    @Override
    public void onCancel(DialogInterface dialogInterface){
        super.onCancel(dialogInterface);
        Log.i("Script:", "onCancel");

    }
  /*  @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        super.onCreateDialog(savedInstanceState);
        Log.i("Script:", "onCreateDialog");
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity()).
                setTitle("Dialog Gragment").setIcon(R.drawable.malaxxhdpi).
                setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getActivity(),"OK",Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       dismiss();
                    }
                });

        return (alert.show());

    }*/

    @Override
    public void onDestroyView( ){
        super.onDestroyView();
        Log.i("Script:", "onDestroyView");
    }
    @Override
    public void onDetach( ){
        super.onDetach();
        Log.i("Script:", "onDetach");
    }
    @Override
    public void onDismiss(DialogInterface dialogInterface){
        super.onDismiss(dialogInterface);
        Log.i("Script:", "onDismiss");
    }
    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        Log.i("Script:", "onDismiss");
    }
    @Override
    public void onStart( ){
        super.onStart();
        Log.i("Script:", "onStart");
    }
    @Override
    public void onStop( ){
        super.onStop();
        Log.i("Script:", "onStop");
    }


}
