package com.carlosdavid.appmis.Entidades;

/**
 * Created by Carlos David on 25/03/2016.
 */
public class Usuario {
    private String nomeUsuario;
    private String senha;
    private String numeroTell;

    public Usuario() {
        super();
    }

    public Usuario(String nomeUsuario, String senha, String numeroTell) {
        this.nomeUsuario = nomeUsuario;
        this.senha = senha;
        this.numeroTell = numeroTell;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public String getSenha() {
        return senha;
    }

    public String getNumeroTell() {
        return numeroTell;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public void setNumeroTell(String numeroTell) {
        this.numeroTell = numeroTell;
    }
}
